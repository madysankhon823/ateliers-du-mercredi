/*
    Créer un formulaire d'inscription d'etudiants qui va prendre les informations suivantes:
        _Nom;
        _Prénom;
        _Date de naissance;
        _Adresse;
        _Email
        _Numéro de tel
        _Note de composition
        _Note de devoir
        _Moyenne
    Le formulaire doit être fait en html css
    Afficher les informations des étudiants sous le formulaire dans un tabléau borduré

    Consigne:
        le formulaire doit avoir 2 boutton, un boutton envoyer et boutton annuler si toute fois on se trompe pour réinitialiser le formulaire
        vous devrez afficher minimum 10 étudians dans le tableau
*/

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.0/firebase-app.js";
import { getFirestore, addDoc, doc, collection, query, getDocs, onSnapshot } from "https://www.gstatic.com/firebasejs/9.12.0/firebase-firestore.js";

const firebaseConfig = {
  apiKey: "AIzaSyC8GRUA8RiExnwSYRRroxtD5McRMyCWECI",
  authDomain: "activites-mercredi.firebaseapp.com",
  projectId: "activites-mercredi",
  storageBucket: "activites-mercredi.appspot.com",
  messagingSenderId: "638496474455",
  appId: "1:638496474455:web:28e89b6d5d20a31afcaa12",
  measurementId: "G-308999XGZT"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const myForm = document.querySelector('form');
const btnSubmit = document.getElementById('submit');
const btnReset = document.getElementById('reset');

document.querySelectorAll('.note').forEach( note =>{
  note.addEventListener('input', ()=>{
    document.getElementById('moyenne').value = ((parseFloat(document.getElementById('n-compo').value) + parseFloat(document.getElementById('n-devoir').value)) / 2)
  })
})

btnSubmit.addEventListener('click', (e) =>{
  e.preventDefault();
  
  addDoc(collection(db, "Etudiants"), {
      prenom: document.getElementById('prenom').value,
      Nom: document.getElementById('nom').value,
      d_naissance: document.getElementById('d-naissance').value,
      adresse: document.getElementById('adresse').value,
      email: document.getElementById('email').value,
      telephone: document.getElementById('tel').value,
      note_Composition: Number(document.getElementById('n-compo').value),
      note_devoir: Number(document.getElementById('n-devoir').value),
      moyenne: Number(document.getElementById('moyenne').value)
    }).then(()=>document.location.reload())  
})

const querySnapshot = await getDocs(collection(db, "Etudiants"));

const datas = [];

querySnapshot.forEach((etudiant) => {
  datas.push({id : etudiant.id, ...etudiant.data()})
});

// const q = query(collection(db, "Etudiants"));
// const unAction = onSnapshot(q, (querySnapshot) => {
//   querySnapshot.forEach((doc) => {
//     datas.push({ id: doc.id, ...doc.data()});
//   });
// });

// console.log(datas, unAction);


// la fonction ---unAction---, une fois appélé, arrete l'ecoute de l'evenement

const tableau = datas.map((elt, index) =>
`<tr>
  <th scope="row">${index+1}</th>
  <td>${elt.prenom}</td>
  <td class="text-uppercase">${elt.Nom}</td>
  <td>${elt.d_naissance}</td>
  <td>${elt.adresse}</td>
  <td class="text-lowercase">${elt.email}</td>
  <td>${elt.telephone}</td>
  <td>${elt.note_Composition}</td>
  <td>${elt.note_devoir}</td>
  <td>${elt.moyenne}</td>
</tr>`
)
tableau.forEach(element => document.getElementById('myTbody').innerHTML += element );