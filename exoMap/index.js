
const tBody = document.getElementById("tBody");
const tBodyArchived = document.getElementById("tBodyArchived");
const eltsArchive = document.querySelector('#eltsarchive');
const search = document.getElementById("search");
const container = document.getElementById("container");
const error = document.getElementById("error");
const myLine = document.getElementsByTagName("tr");
eltsArchive.style.display = 'block'
let myId = 0;




const tableau = [
  { Prenom: "Omar", Nom: "sylla", Age: 16, Note: 12, id: myId++, isArchiver: false},
  { Prenom: "Mamadou", Nom: "touré", Age: 17, Note: 2, id: myId++,  isArchiver: false},
  { Prenom: "Alpha", Nom: "camara", Age: 15, Note: 13, id: myId++,  isArchiver: false},
  { Prenom: "Oumane", Nom: "sonko", Age: 18, Note: 14, id: myId++,  isArchiver: false},
  { Prenom: "Maimouna", Nom: "diouf", Age: 20, Note: 7, id: myId++,  isArchiver: false},
  { Prenom: "Amadou", Nom: "diop", Age: 15, Note: 11, id: myId++,  isArchiver: false},
  { Prenom: "Fatoumata", Nom: "diallo", Age: 15, Note: 19, id: myId++,  isArchiver: false},
  { Prenom: "Aminata", Nom: "dieng", Age: 15, Note: 16.5, id: myId++,  isArchiver: false},
  { Prenom: "Ousseynou", Nom: "touré", Age: 17, Note: 3, id: myId++,  isArchiver: false},
  { Prenom: "Hamidou", Nom: "diop", Age: 16, Note: 17.5, id: myId++,  isArchiver: false},
];

// Pour sauvegarder mon tableau dans le localStorage

function saveTable(arg) {
  localStorage.setItem("monTableau",JSON.stringify(arg))
}

// récuperer le tableau dans le localStorage
let localTable;

function getTable() {
  localTable = localStorage.getItem("monTableau");
  if (localTable === null) {
    return [];
  }else{
    return JSON.parse(localTable);
  }
}

localTable = getTable();




const notArchived = localTable.filter(elts => elts.isArchiver === false);
const Archived = localTable.filter(elts => elts.isArchiver === true);

console.log(localTable);
console.log(tab);
console.log(notArchived);


document.getElementById('ajouter').addEventListener('click',(e) =>{
  // let localTable = getTable()
  const ajouterObj = {}
    ajouterObj.Prenom = prompt(`saisissez le prénom de l'étudiant`);
    ajouterObj.Nom = prompt(`saisissez le Nom de l'étudiant`);
    ajouterObj.Age = parseInt(prompt(`saisissez l'age de l'étudiant`));
    ajouterObj.Note = parseInt(prompt(`saisissez la note de l'étudiant`));
    ajouterObj.id = myId++;
    ajouterObj.isArchiver = false;

    
    //  tableau.push(ajouterObj)
    //  afficher(tab,tBody);
    
    // return this;
    // console.log(ajouterObj);
    localTable.push(ajouterObj);
    saveTable(localTable);
    location.reload();
    // afficher(tab,tBody);
    return this;
    
  });
  
document.getElementById('reset').addEventListener('click',()=> {
  saveTable(tableau)
  location.reload()
})

function afficher(my_Table, my_TableId){
  const table = my_Table.map(
    (etudiant) =>
      `<tr id = rowTableId${etudiant.id}>
          <td>${etudiant.Prenom}</td>
          <td class = "nom">${etudiant.Nom}</td>
          <td>${etudiant.Age}</td>
          <td>${etudiant.Note}</td>
          <td>
              <button name='edit' value = "${etudiant.id}" type = "button" id = "btnEditId${etudiant.id}" ><i class="fa-solid fa-file-pen"></i></button>
              <button name='archiver' type = "button" id = "${etudiant.id}"><i class="${etudiant.isArchiver === false ? 'fa-solid fa-folder':'fa-regular fa-folder-open'}"></i></button>
              <button name='suppression' value = "${etudiant.id}" type = "button" id = "btnEditId${etudiant.id}"><i class="fa-solid fa-trash-can"></i></button>
          </td>
      </tr>`
  );
  table.forEach((elt) => (my_TableId.innerHTML += elt));

  return this;
}
// function afficherArchiver(my_Table, my_TableId){
//   const table = my_Table.map(
//     (etudiant) =>
//       `<tr id = rowTableId${etudiant.id}>
//           <td>${etudiant.Prenom}</td>
//           <td class = "nom">${etudiant.Nom}</td>
//           <td>${etudiant.Age}</td>
//           <td>${etudiant.Note}</td>
//           <td>
//               <button name='edit' value = "${etudiant.id}" type = "button" id = "btnEditId${etudiant.id}" ><i class="fa-solid fa-file-pen"></i></button>
//               <button name='archiver' bool = "true" value = "${etudiant.id}" type = "button" id = "btnEditId${etudiant.id}"><i class="fa-regular fa-folder-open"></i></button>
//               <button name='suppression' value = "${etudiant.id}" type = "button" id = "btnEditId${etudiant.id}"><i class="fa-solid fa-trash-can"></i></button>
//           </td>
//       </tr>`
//   );
//   table.forEach((elt) => (my_TableId.innerHTML += elt));

//   return this;
// }

const plusGrandeNote = Math.max.apply(
  null,
  tab.map((elt) => elt.Note)
);
const plusPetiteNote = Math.min.apply(
  null,
  tab.map((elt) => elt.Note)
);
const plusAgee = Math.max.apply(
  null,
  tab.map((elt) => elt.Age)
);
const moinAgee = Math.min.apply(
  null,
  tab.map((elt) => elt.Age)
);
container.innerHTML = `<p>La plus grande note est: ${plusGrandeNote}</p>
<p>La plus petite note est: ${plusPetiteNote}</p>
<p>La plus agée est: ${plusAgee}</p>
<p>La moin agée est: ${moinAgee}</p>`;

afficher(notArchived,tBody);
afficher(Archived,tBodyArchived);


document.querySelectorAll('button').forEach(button => {

    button.addEventListener('click', () => {
      let td= button.parentNode;
      let tr = td.parentNode;
      let tbody = tr.parentNode;
      const position = parseInt(button.value);
      const myIdPosition = localTable.find(elts => elts.id === position);

      
        // console.log('je suis ', button.name);
        if (button.name === 'edit') {
          myIdPosition.Prenom = prompt(`saisissez le prénom de l'étudiant`, myIdPosition.Prenom);
          myIdPosition.Nom = prompt(`saisissez le Nom de ${myIdPosition.Prenom}`, myIdPosition.Nom);
          myIdPosition.Age = parseInt(prompt(`saisissez l'age de ${myIdPosition.Prenom}`, (myIdPosition.Age).toString()));
          myIdPosition.Note = parseInt(prompt(`saisissez la note de ${myIdPosition.Prenom}`, (myIdPosition.Note).toString()));
          saveTable(localTable);
          location.reload()
        }
        else if (button.name === 'archiver') {
          if (myIdPosition.isArchiver === false) {
            myIdPosition.isArchiver = true;
          } else {
            myIdPosition.isArchiver = false;
          }
          saveTable(localTable);
          location.reload();
        }
        else if(button.name === 'suppression'){

          localTable = localTable.filter(elts => elts.id !== position )
          
            saveTable(localTable);
            location.reload();
        }
      return this;
    })
})
search.addEventListener("input", (e) => {
  let myResaerch,
    cellule0,
    cellule1,
    cellule2,
    cellule3;
  myResaerch = e.target.value.toLowerCase();

  for (let i = 0; i < myLine.length; i++) {
    cellule0 = myLine[i].getElementsByTagName("td")[0];
    cellule1 = myLine[i].getElementsByTagName("td")[1];
    cellule2 = myLine[i].getElementsByTagName("td")[2];
    cellule3 = myLine[i].getElementsByTagName("td")[3];
    if (cellule0 || cellule1 || cellule2 || cellule3) {
      let texte0 = cellule0.innerHTML;
      let texte1 = cellule1.innerHTML;
      let texte2 = cellule2.innerHTML;
      let texte3 = cellule3.innerHTML;
      if (
        texte0.toLowerCase().indexOf(myResaerch) > -1 ||
        texte1.toLowerCase().indexOf(myResaerch) > -1 ||
        texte2.toLowerCase().indexOf(myResaerch) > -1 ||
        texte3.toLowerCase().indexOf(myResaerch) > -1
      ) {
        myLine[i].style.display = "";
        error.innerHTML = "";
      } else {
        myLine[i].style.display = "none";
        error.innerHTML = `<p>Aucun autre élément trouver</p>`;
      }
    }
  }
});

// const MyArchiveFunc = (e) =>{
//   console.log(`Bonjour le monde je vais être archiver et voico mon id: ${this.id}`)
// }

// const MyDeleteFunc = (e) =>{
//   console.log(`Bonjour le monde je vais être supprimer et voico mon id: ${this.id}`)
// }
// afficher(tableau, tBody)



