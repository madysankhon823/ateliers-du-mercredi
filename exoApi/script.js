/*
Exercice:

1) Ecrire un programme js qui permet d'afficher 10 elements avec une API  en ligne la méthode axios.get
2) C'est la suite de l'exercice N° 1, toujours en utilisant la même API en ligne, cette fois ci les 
    données seront afficher sous forme de tableau;
  _Ajouter les fonctionnalités post et delete
    _Pour la fonctionnalité post, utiliser les informations de la table de l'API
    _

 Partager les exercices des atéliers sur gitLab à: kalikaba9@gmail.com
 Postman: est outil qui nous permet de tester nos API

*/
// const axios = require('axios').default;

// async function getUser() {
//   try {
//     const response = await axios.get('https://dummyjson.com/users?limit=5');
//     console.log(response);
//   } catch (error) {
//     console.error(error);
//   }

// import { axios } from 'axios';
// const axios = require('axios').default;

  axios.get('https://dummyjson.com/users?limit=10')
//  axios.get('https://jsonplaceholder.typicode.com/todos/1')

  // reponse.then(elt => elt.json())
  .then(elt => elt.data)
  .then(response => console.log(response))


// const getUser = () => {
  // const response = axios.get('https://dummyjson.com/users?limit=5')

  // const données = response.then(elt => elt.data)

  // response.then(elts => console.log(elts))
// }

// getUser()


