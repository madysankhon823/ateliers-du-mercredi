const tableau = document.querySelector('#table');
const tBody = document.querySelector('#tbody');
const tbodyArchiver = document.getElementById('tbodyArchiver');
const envoyer = document.getElementById('send');
const modidier = document.getElementById('modidier');
let prenom = document.querySelectorAll('input')[0];
let nom = document.querySelectorAll('input')[1];
let date = document.querySelectorAll('input')[2];
let lieu = document.querySelectorAll('input')[3];
let nbrArchiver = document.querySelector('#archiver');
let nbrRetenu = document.querySelector('#retenu');
let nbrArchiver1 = document.querySelector('#archiver1');
let nbrRetenu1 = document.querySelector('#retenu1');
let rechercheNonArchiver = document.querySelector('#rechercheNonArchiver');
let rechercheArchiver = document.querySelector('#rechercheArchiver');
const closeModal = document.querySelectorAll('.table');
// const archiveModal = document.querySelector('.modal-container');


console.log(tBody);

// Pour sauvegarder mon tableau dans le localStorage

function saveTable(arg) {
    localStorage.setItem("monTableauLocal",JSON.stringify(arg))
}

// récuperer le tableau dans le localStorage
    let localTable;
  
function getTable() {
    localTable = localStorage.getItem("monTableauLocal");
    if (localTable === null) {
      return [];
    }else{
      return JSON.parse(localTable);
    }
  }
  
// Pour filtrer mes éléments archiver ou non, et le nombre d'élément
  
  localTable = getTable();
  
  const notArchiver = localTable.filter(elts => elts.isArchiver === false);
  const Archiver = localTable.filter(elts => elts.isArchiver === true);

  console.log(Archiver);

  function nbrElements(archiver, retenu) {
    if (archiver === nbrArchiver && retenu === nbrRetenu) {
      archiver.innerHTML = Archiver.length;
      if (notArchiver.length > 1) {
        retenu.innerHTML = `${notArchiver.length} retenus`;
      } else {
        retenu.innerHTML = `${notArchiver.length} retenu`;
      }
    } else {
      [archiver, retenu] = [retenu, archiver]
      retenu.innerHTML = notArchiver.length;
      if (notArchiver.length > 1) {
        archiver.innerHTML = `${Archiver.length} archivés`;
      } else {
        archiver.innerHTML = `${Archiver.length} archivé`;
      }
    }
  }

  nbrElements(nbrArchiver,nbrRetenu);
  nbrElements(nbrRetenu1,nbrArchiver1);
  
// ----------------afficher mes tableaux--------------------//

// ---Créeation de ma fonction afficher--- //

function Afficher(tableJs, idTBody){
  const table = tableJs.map(
    (inscrit, index) =>
      `<tr id = ${inscrit._id}>
            <td>${index + 1}</td>
            <td>${inscrit.prenom}</td>
            <td>${inscrit.nom}</td
            >
            <td>${inscrit.date}</td>
            <td>${inscrit.lieu}</td>
            <td>
              <button name='edit' type = "button" id = "${inscrit._id}" ><i class="fa-solid fa-pen"></i></button>
              <button name='archiver' type = "button" id = "${inscrit._id}"><i class="${inscrit.isArchiver === false ? 'fa-solid fa-folder':'fa-regular fa-folder-open'}"></i></button>
              <button name='suppression' type = "button" id = "${inscrit._id}"><i class="fa-solid fa-trash-can"></i></button>
          </td>
      </tr>`
    );
 return table.forEach((elt) => (idTBody.innerHTML += elt))
}

// --- L'appelle de ma  fonction pour afficher les 2 tableaux --- //

// Afficher(localTable, tBody)

// Mes elements non archiver

Afficher(notArchiver, tBody);

// Mes éléments archicher

Afficher(Archiver, tbodyArchiver);


// --- Ma fonction rechercher

function rechercher(myIdRecherche, myIdTbody) {
  
  myIdRecherche.addEventListener('input', (e) =>{
    let myLine;
    if (myIdTbody === rechercheNonArchiver) {
      myLine = document.querySelectorAll('#tbody tr');
    } else if (myIdTbody === rechercheArchiver) {
      myLine = document.querySelectorAll('#tbodyArchiver tr');
    }
    let inputValue, cellule0, cellule1, cellule2, cellule3, cellule4;
  
    inputValue = e.target.value.toLowerCase();
  
    for (let i = 0; i < myLine.length; i++) {
      cellule0 = myLine[i].getElementsByTagName('td')[0];
      cellule1 = myLine[i].getElementsByTagName('td')[1];
      cellule2 = myLine[i].getElementsByTagName('td')[2];
      cellule3 = myLine[i].getElementsByTagName('td')[3];
      cellule4 = myLine[i].getElementsByTagName('td')[4];
  
      if (cellule0 || cellule1 || cellule2 || cellule3 || cellule4) {
        let text0 = cellule0.innerHTML;
        let text1 = cellule1.innerHTML;
        let text2 = cellule2.innerHTML;
        let text3 = cellule3.innerHTML;
        let text4 = cellule4.innerHTML;
  
        if (
          text0.toLowerCase().indexOf(inputValue) > -1 ||
          text1.toLowerCase().indexOf(inputValue) > -1 ||
          text2.toLowerCase().indexOf(inputValue) > -1 ||
          text3.toLowerCase().indexOf(inputValue) > -1 ||
          text4.toLowerCase().indexOf(inputValue) > -1
        ) {
          myLine[i].style.display = "";
        } else {
          myLine[i].style.display = "none";
        }
      }
    }
    console.log("test tableau: ", localTable);
  })
}
rechercher(rechercheArchiver, rechercheArchiver);
rechercher(rechercheNonArchiver, rechercheNonArchiver);



document.querySelectorAll('button').forEach(button =>{
  button.addEventListener('click',(e) => {

    const myIdPosition = localTable.find(elts => elts._id === button.id);

    console.log(myIdPosition);
    


    if (button.name === 'edit') {
      envoyer.style.display = 'none'
      modidier.style.display = 'block'
      
      document.querySelectorAll('input')[0].value = myIdPosition.prenom;
      document.querySelectorAll('input')[1].value = myIdPosition.nom;
      document.querySelectorAll('input')[2].value = myIdPosition.date;
      document.querySelectorAll('input')[3].value = myIdPosition.lieu;

      
      modidier.addEventListener('click', () => {
        
        let prenom = document.querySelectorAll('input')[0].value;
        let nom = document.querySelectorAll('input')[1].value;
        let date = document.querySelectorAll('input')[2].value;
        let lieu = document.querySelectorAll('input')[3].value;  
        
        myIdPosition.prenom = prenom;
        myIdPosition.nom = nom;
        myIdPosition.date = date;
        myIdPosition.lieu = lieu;
        
        // console.log(prenom, nom, lieu, date);
        console.log(myIdPosition);
        saveTable(localTable)
        // location.reload
        e.preventDefault();
      })
    } else if(button.name === 'archiver') {
      if (myIdPosition.isArchiver === false) {
        myIdPosition.isArchiver = true
      } else {
        myIdPosition.isArchiver = false
      }
      saveTable(localTable)
      location.reload()
    } else {
      localTable = localTable.filter(elts => elts._id !== button.id);
      saveTable(localTable);
      location.reload();
    }
  })
})
envoyer.addEventListener('click', () => {
  const prenom = document.querySelectorAll('input')[0].value;
  const nom = document.querySelectorAll('input')[1].value;
  const date = document.querySelectorAll('input')[2].value;
  const lieu = document.querySelectorAll('input')[3].value;
  const myObj = {};

  // let myId = Math.max.apply(null, localTable.map(elt => elt._id));

  // if (myId === -Infinity) {
  //   myId = 0
  // }
  
    myObj._id = new Date();
    myObj.prenom = prenom;
    myObj.nom = nom;
    myObj.date = date;
    myObj.lieu = lieu;
    myObj.isArchiver = false
    
    console.log(prenom, nom, date, lieu);
    localTable.push(myObj);
    saveTable(localTable);
    console.log(localTable);
    return this;
});


closeModal.forEach(btnClose => btnClose.addEventListener('click', (e) =>{
  // archiveModal.classList.toggle("active");
  e.preventDefault();
}))
console.log(closeModal);
